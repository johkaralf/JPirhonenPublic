// Moudule: mainwindow / file: mainwindow.hh
// The header file for the class definitions of the mainwindow GUI:
// includes declarations of the classes, actions and needed in the source code for
//the main window.
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>


namespace Ui {
class MainWindow;
}

// Classes needed for the creation of the exclusive gender menu.
class QAction;
class QActionGroup;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    // Resets all BMI calculator values and sliders and the gender to female. Then the
    // reset action from the action menu is clicked.
    void reset_action_triggered();

    // Calculates and displays the BMI as a double according to weight and height.
    void display_bmi();

private slots:
    // Sets the gender text label to display the gender selected.
    void setGenderFemale();
    void setGenderMale();

private:
    // Private functions to create the gender menu.
    void createActions();
    void createMenus();

    // String returning functions that return the adequate text interpretation of the
    // BMI according to the gender selected.
    QString female_bmi_interpretation(double BMI);
    QString male_bmi_interpretation(double BMI);

    Ui::MainWindow *ui;

    // Declaration of the gender menu and its exclusive action group and actions.
    QMenu *menuGender;
    QActionGroup *genderGroup;
    QAction *setGenderFemaleAct;
    QAction *setGenderMaleAct;
};


#endif // MAINWINDOW_HH
