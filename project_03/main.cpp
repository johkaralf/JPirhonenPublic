//------------------------------------------------------------------------//
//                                                                        //
// TIE-02206 Basic Course on Programming, S2017                           //
//                                                                        //
// Programmer:  Johannes Pirhonen                                         //
// Student ID:  224613                                                    //
// E-mail:      johannes.pirhonen@student.tut.fi                          //
// Work:        project-03                                                //
// Modified:    23.11.2017                                                //
//                                                                        //
// This program is an implementation of a graphical user interface of a   //
// Body Mass Index calculator. The module mainwindow creates a window,    //
// where the user can input his or her height and weight either manually  //
// in the two dedicated spin boxes or by sliders. A gender specific text  //
// interpretation of the calculated BMI is provided by choosing the right //
// gender. There are two menus, one with reset and quit actions and one   //
// exclusive for the gender selection.                                    //
//                                                                        //
//------------------------------------------------------------------------//
#include "mainwindow.hh"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();

}
