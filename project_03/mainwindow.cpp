// Moudule: mainwindow / file: mainwindow.cpp
// The source file for the class and method definitions of the mainwindow GUI:
// includes implementations of the actionGroup for the gender menu, the reset action,
// BMI calculation, gender changes, and gender specific text interpretation of the BMI.
// Author:
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include <math.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Whenever the weight value changes, the BMI is newly calculated and diaplayed.
    connect(ui->weight_spinbox,SIGNAL(valueChanged(int)),
            this, SLOT(display_bmi()));

    // Whenever the height value changes, the BMI is newly calculated and displayed.
    connect(ui->height_spinbox,SIGNAL(valueChanged(int)),
            this, SLOT(display_bmi()));

    // If the reset action is clicked, the reset_action_triggeret slot function is called.
    connect(ui->actionReset,SIGNAL(triggered(bool)),
            this, SLOT(reset_action_triggered()));

    // The window is closed if the quit action is clicked.
    connect(ui->actionQuit,SIGNAL(triggered(bool)),
            this, SLOT(close()));

    // Private methods to create the exclusive gender menu (actionGroup).
    createActions();
    createMenus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{
    // The female action is created and made checkable for the gender menu.
    setGenderFemaleAct = new QAction(tr("Female"), this);
    setGenderFemaleAct->setCheckable(true);

    // When the gender female is selected, the gender label and BMI are updated accordinly.
    connect(setGenderFemaleAct, SIGNAL(triggered(bool)),
            this, SLOT(setGenderFemale()));
    connect(setGenderFemaleAct, SIGNAL(triggered(bool)),
            this, SLOT(display_bmi()));

    // The male action is created and made checkable for the gender menu.
    setGenderMaleAct = new QAction(tr("Male"), this);
    setGenderMaleAct->setCheckable(true);

    // When the gender male is selected, the gender label and BMI are updated accordinly.
    connect(setGenderMaleAct, SIGNAL(triggered(bool)),
            this, SLOT(setGenderMale()));
    connect(setGenderMaleAct, SIGNAL(triggered(bool)),
            this, SLOT(display_bmi()));

    // The gender actionGroup is created and the gender actions added.
    // Female is selected as default.
    genderGroup = new QActionGroup(this);
    genderGroup->addAction(setGenderFemaleAct);
    genderGroup->addAction(setGenderMaleAct);
    setGenderFemaleAct->setChecked(true);
}


// Gender menu is created.
void MainWindow::createMenus()
{
    menuGender = menuBar()->addMenu(tr("Gender"));
    menuGender->addAction(setGenderFemaleAct);
    menuGender->addAction(setGenderMaleAct);
}

// Sets the gender text label to display female and invokes the setChecked method for
// checking the female option in the gender menu (for reset purposes).
void MainWindow::setGenderFemale()
{
    ui->gender_label->setText("Female");
    setGenderFemaleAct->setChecked(true);
}

// Sets the gender text label to display male.
void MainWindow::setGenderMale()
{
    ui->gender_label->setText("Male");
}

// The Body Mass Index is calculated if both weight and height are non zero and the gender
// specifec *_bmi_interpretation() is called.
void MainWindow::display_bmi()
{
    double weight_kg;
    double height_m;
    double bmi;
    weight_kg = ui->weight_spinbox->value();
    height_m = ui->height_spinbox->value();

    if ((ui->weight_spinbox->value() > 0) && (ui->height_spinbox->value() > 0)) {

        bmi = weight_kg / ((pow(height_m, 2))/10000);

        ui->bmi_label->setNum(bmi);

        // BMI interpretation for females
        if (ui->gender_label->text() == "Female") {
            ui->interpretation_label->setText(female_bmi_interpretation(bmi));

        // BMI interpretation for males
        } else {
            ui->interpretation_label->setText(male_bmi_interpretation(bmi));
        }

    } else {
        ui->bmi_label->setText("-");
        ui->interpretation_label->setText("-");
    }


}

// Case switch to handle the different BMI interpretations using a mathematical calculation
// inspired by Lajos Arpad: https://stackoverflow.com/questions/33037832/range-checks-using-a-switch-statement.
// Each comparison operand returns either a literal 1 or 0 if the comparison is true or faflse respectively.
// Parameters:
//      Calculated Body Mass Index as a double
// Returns:
//      Text representation of the BMI as a QString.
QString MainWindow::female_bmi_interpretation(double BMI)
{
    switch (1 + (BMI > 19.1) + (BMI > 25.8) + (BMI > 27.3) + (BMI > 32.2)) {
        case 1: return "Underweight";
        case 2: return "Normal";
        case 3: return "Slight overweight";
        case 4: return "Overweight";
        case 5: return "Much overweight";
    default:
        return "Switch error!";
    }
}

QString MainWindow::male_bmi_interpretation(double BMI)
{
    switch (1 + (BMI > 20.7) + (BMI > 26.4) + (BMI > 27.8) + (BMI > 31.1)) {
        case 1: return "Underweight";
        case 2: return "Normal";
        case 3: return "Slight overweight";
        case 4: return "Overweight";
        case 5: return "Much overweight";
    default:
        return "Switch error!";
    }
}

// When reset is triggered, the values of weight and height are reset to zero and the gender is set to female.
void MainWindow::reset_action_triggered()
{
    ui->weight_spinbox->setValue(0);
    ui->height_spinbox->setValue(0);
    setGenderFemale();
}
