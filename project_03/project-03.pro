#-------------------------------------------------
#
# Project created by QtCreator 2017-11-12T21:17:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = project-03
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.hh

FORMS    += mainwindow.ui
