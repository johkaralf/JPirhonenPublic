/***************** Pesävaa'an tulokset ja läpötila tekstiviestillä **************

TIETOJA:
3,6864MHz!!
 toimii.
EI SSL-tukea lähettimessä 3!

 muista kalibroida readVcc() kullekin kontrollerille.

 VERSIOSSA UUTTA:
 debugviesti: #d lähettää normaalin viestin tilalla debugveistin ylläpitäjälle
 Tuki 8MHz kiteelle.
 EEPROM
 saldo
 Huomioidaan tilanne, jossa lähetin herää ennen haluttua aikaa, ja siksi lähettää kaksi viestiä peräkkäin:
 ensimmäisen kerran herätessään, ja muutaman minuutin päästä uudelleen kun koittaa haluttu_aika!!
   (if (uni_minuuteissa < 61){uni_minuuteissa += 1439;})
 
 25.4.17: korjattu if if -> if elif
 
 26.4.17: Email-toiminto
 29.4.17: laaja kenttätieto pois, kysy_kentta_saldo_debug
 30.4.17: -säätö nukkumisintervallille.
          -muutettu "interval" -> "uni_minuuteissa"
 3.4.17:  -tekstiviestillä vaihdettava puh_admin jolle debugviesti tulee. viesti ja debug lähtee molemmat (elif -> if)
          -uni_minuuteissa sievennetty yhteen kaavaan
 14.5.17: -rekisteröity smtp2go palveluun ja ohjattu posti portin 2525 kautta. ks "Sähköpostiviestin lähetys" -osuus. tämän lähäettimen 
           moduulissa ei SSL-tukea. Huom smtp2go palvelun ilmaisen tilin rajoitteet 1000 viestiä kuussa, 25 tunnissa.
           smtp-tilin auth: samat kuin hanan gmail.
 15.5.17: -korjattu ongelma joka sotkee eepromin kun tekstiviestikomennon lopetusmerkki unohtuu. lopetusmerkeiksi asetettu välilyönti, risuaita ja 0x0D.
          -korjattu ongelma jolloin eepromiin tallennetaan edellistä lyhyempi merkkijono: kirjoitetaan muistipaikat ensin kaikki 0x00. katso "clear_eeprom".
          -korjattu väärä haluttu aika debugviestistä ja poistettu debugviestistä "verkon aika".
          
 14.7.17: -Poistettu turhia eeprommeja ja asetettu ne vakioiksi flashiin: laitteen nimi, puh_admin
 VIELÄ TEHTÄVÄÄ:
 
 EEPROMMIEN ALUSTUS!! Jos kirjotetaan lyhyempi muuttuja kuin edellinen muistissa ollut, vanhan jämät jää roikkumaan!
 
 tiivistä true/false kyselyt bittimaskeille muutamaan tavuun. niinkuin tallenna_eeprom!

 http://stackoverflow.com/questions/16055156/receiving-sms-using-gsm-and-controlling-led-using-arduino
 kontrollerin unitila: digitaalipinnit ja ADC toggle erikseen?
 

 pienennä ohjelman kokoa jotta mahtuu atTinyyn. 8kb?: floatit, global  variablet, etenkin swSerial.
 yhdistämällä esim confirm-funktioita?

Lyijyakun jännitteen tarkempi lukeminen 

MUISTILISTA:
katso ohjelmointi_muistilista.txt!
  
 */

#include <SoftwareSerial.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>

//#define F_CPU 3686400L
#define F_CPU 8000000L
//#define F_CPU 16000000L
// Termistoripinnin mumero
#define THERMISTORPIN A1         
// resistanssi 25 C asteessa
#define THERMISTORNOMINAL 100000      
// nimellisresistanssin lämpötila
#define TEMPERATURENOMINAL 25   
// ADC-näytteiden määrä.
#define NUMSAMPLES 100
// termistorin beta-kerroin
#define BCOEFFICIENT 3942
// Kiinteän sarjavastuksen arvo lämpötilanmittauksessa
#define SERIESRESISTOR 100000
// Akun varaustilan mittaukseen vastusjako
#define R1 1000.0
#define R2 820.0

//EEPROM declarationit EI TOIMI OIKEIN ARDUINO-YMPÄRISTÖSSÄ: alkuarvot eivät tallennu tässä. 
//ainoastaan alustuvat tyhjiksi muistipaikoiksi. tässä koodissa lähetetään ne tekstiviestillä.
//Katso poimi_komennot_viesteista-funktio.
//
char versio[] = "0.5.3";           
char fcpu[] = "8 Mhz";             
char sarjanro[] = "5";              
char laitteen_nimi[] = "Vaaka 05";  

//char EEMEM EEnimi[9];
char EEMEM EEpuh[11];
//char EEMEM EEadmin_puh[11];
//char EEMEM EEsmtp_palvelin[31];
//char EEMEM EElahettaja_sposti[31];
//char EEMEM EElahettaja_salasana[31];
//char EEMEM EElahettaja_nimi[31];
char EEMEM EEvastaanottaja_sposti[31];
//char EEMEM EEvastaanottaja_nimi[31];
uint16_t EEMEM EEaika;
uint16_t EEMEM EElahetystapa; // nollas bitti 0x01 tekstiviestille, seuraava bitti spostille


char puh_admin[]= "0407244986";
char puh_vastaanottaja[11];
char palvelin_smtp[31] = "smtpa.kolumbus.fi";
char sposti_lahettaja[32] = "hunajahana@saunalahti.fi";
char salasana_lahettaja[31] = "m4m1t412";
char nimi_lahettaja[21] = "Hunajahana";
char sposti_vastaanottaja[32];
//char nimi_vastaanottaja[31];
uint16_t haluttu_aika;
uint16_t haluttu_lahetystapa = 0x00;
uint8_t clear_eeprom[31] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

char ReceivedByte = 0x00;
int samples[NUMSAMPLES];
uint8_t i;
uint8_t j;
float average;
uint16_t V_patterit;
char kayttojannite[4];
byte adcsra;
char verkon_tunnit[3];
char verkon_minuutit[3];
char haluttu_tunnit[3];
char haluttu_minuutit[3];
uint16_t verkon_aika = 0; //verkon aika minuuteissa 0-1439
char verkon_aika_koe[6]; 
volatile int sleep_count = 0; // Keep track of how many sleep cycles (about 8s) have been completed. Volatile because it is changed inside an ISR
unsigned int uni_minuuteissa = 1439; // sleep time in minutes
unsigned int edellinen_uni_minuuteissa = 1439;
unsigned int sleep_total; // Approximate number of sleep cycles needed before the uni_minuuteissa defined above elapses.
char saldo[7];
char voim_oloaika[12];
//uint8_t kysy_kentta = 0;
//uint8_t kysy_saldo = 0;
//uint8_t laheta_debug = 0;
uint8_t kysy_kentta_saldo_debug = 0x00; // debug:0x01 saldo:0x02 kenttä:0x04

char kentta[3];
uint8_t tallenna_eeprom = 0x00; //kokeillaan käyttää yhden tavun bittejä true/false tarkasteluun.
uint8_t tallenna_eeprom_email = 0x00;
//int looppien_lkm = 0;

/* Kuuluvuuden parametrit suoraan AT-manuaalista
"
<rssi>
0 -115 dBm or less
1 -111 dBm
2...30 -110... -54 dBm
31 -52 dBm or greater
99 not known or not detectable
<ber> (in percent):
0...7 As RXQUAL values in the table in GSM 05.08 [20]
subclause 7.2.4
99 Not known or not detectable
"
*/

SoftwareSerial swSerial(A5, A3); // RX, TX

unsigned char uart_getbyte(void) {
    unsigned long alkuMillis = millis();
    while ((UCSR0A & (1 << RXC0)) == 0) {
        unsigned long nytMillis = millis();
        if(nytMillis - alkuMillis > 10){
            break;
        }
    }  
    return UDR0;
}

void sendAT(char buffer[]){
    
    // wait for empty transmit buffer
    while( !( UCSR0A & (1<<UDRE0)) );            
    //Send data through UART                
    int len = strlen(buffer);
        
    for (i=0; i<len; ++i) {
        // Write 1 byte (1 char) to UART0 TX register
        UDR0 = buffer[i];           
    
        // Wait until TX buffer is empty
        // before sending new byte
        loop_until_bit_is_set(UCSR0A, UDRE0);
    }
}
/*
void sendAT_hex(char buffer[]){    //MUISTA MUUNTAA SOPIVAKSI LÄHETINKOODIIN
    char hexana[5];
      
    // wait for empty transmit buffer
    while( !( UCSR0A & (1<<UDRE0)) );            
    //Send data through UART                
    int len = strlen(buffer);
        for (int i=0; i<len; i++){    //syljetään viesti merkki kerrallaa hexana sarjaporttiin   
            sprintf(hexana,"%X", buffer[i]); // esitä heksadesimaalina
            int len2 = strlen(hexana);
            for (j=0; j<len2; ++j) {
            // Write 1 byte (1 char) to UART0 TX register
            UDR0 = hexana[j];           
    
            // Wait until TX buffer is empty
            // before sending new byte
            loop_until_bit_is_set(UCSR0A, UDRE0);
        }
    }
}
*/

void confirmAT(void){
    j = 0;
    unsigned long alkuMillis = millis();
    char edellinen = 0x00;
    while(j < 3){
        unsigned long nytMillis = millis();
        // Handle AT-command response
        ReceivedByte = uart_getbyte();

        if ((edellinen == 'O') & (ReceivedByte == 'K')){
            ++j;   
        }
        else if ((j == 1) & (ReceivedByte == 0x0D)){
            ++j;   
        }
        else if ((j == 2) & (ReceivedByte == 0x0A)){
            ++j;   
        }
        else if ((nytMillis - alkuMillis) > 10000){
                //sendAT("GSM ei vastaa: confirmAT\r\n");
                break;                
        }
        edellinen = ReceivedByte;
    }
}
void confirmEMAIL(void){
    unsigned long alkuMillis = millis();
    while(!(uart_getbyte() == '+')){ // Odottaa "+" (+SMTPSEND: 1)
        unsigned long nytMillis = millis();
        if ((nytMillis - alkuMillis) > 15000){
            break;                
        }
    }
}

void confirmPOWD(void){
    unsigned long alkuMillis = millis();
    j = 0;
    while(j < 4){
        unsigned long nytMillis = millis();
        // Handle AT-command response
        ReceivedByte = uart_getbyte();
        if (ReceivedByte == 'N'){
            ++j;   
        }
        else if ((j == 1) & (ReceivedByte == 'N')){
            ++j;
        }
        else if ((j == 2) & (ReceivedByte == 0x0D)){
            ++j;   
        }
        else if ((j == 3) & (ReceivedByte == 0x0A)){
            ++j;   
        }
        else if ((nytMillis - alkuMillis) > 3000){
            //sendAT("GSM ei vastaa: powd");
            break;                
        }
    }
}


void poimi_komennot_viesteista(void){
    char edellinen = 0x00;
    unsigned long alkuMillis = millis();
    j = 0;
    while(j < 3){
        unsigned long nytMillis = millis();
        ReceivedByte = uart_getbyte();
        /* Laitteen nimen vaihtaminen. Tässä versiossa nimen oltava tasan 8 merkkiä pitkä. */
        /*
        if ((edellinen == '#') & (ReceivedByte == 'n')){
            for (i=0; i<8; i++){
                ReceivedByte = uart_getbyte();
                laitteen_nimi[i] = ReceivedByte;
            }
            tallenna_eeprom |= 0x01; // nollanteen bitiin 1: 00000001
            
        }
        */
        /* Vastaanottajan puhelinnumero */
        //else if ((edellinen == '#') & (ReceivedByte == 'p')){
        if ((edellinen == '#') & (ReceivedByte == 'p')){
            for (i=0; i<10; i++) {
                ReceivedByte = uart_getbyte();
                puh_vastaanottaja[i] = ReceivedByte;       
            }
            tallenna_eeprom |= 0x02;  //ensimmäiseen bittiin 1: 00000010
        }
        /* Ylläpitäjän puhelinnumero 
        else if ((edellinen == '#') & (ReceivedByte == 'y')){
            for (i=0; i<10; i++) {
                ReceivedByte = uart_getbyte();
                puh_admin[i] = ReceivedByte;       
            }
            tallenna_eeprom |= 0x08;  //kolmanteen bittiin 1: 0000 1000
        }
        */
        /* Ajanasetusviestin lukeminen */
        else if ((edellinen == '#') & (ReceivedByte == 'a')){
            for (i=0; i<2; i++){
                ReceivedByte = uart_getbyte();
                haluttu_tunnit[i] = ReceivedByte;
            }
            ReceivedByte = uart_getbyte();
            for (i=0; i<2; i++){
                ReceivedByte = uart_getbyte();
                haluttu_minuutit[i] = ReceivedByte; 
            }
            // jos luettu aika ei välillä 0-1439, ei tallenneta haluttuun aikaan, eikä eepromiin.
            // atoi: string to integer
            uint16_t luettu_aika = ((atoi(haluttu_tunnit))*60) + atoi(haluttu_minuutit);
            if ((luettu_aika >= 0) & (luettu_aika <= 1439)){
                haluttu_aika = luettu_aika;
                tallenna_eeprom |= 0x04; //toiseen bittiin 1: 00000100
            }            
        }
        
        /* SMTP-palvelin */
        else if ((edellinen == '#') & (ReceivedByte == 'P')){
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    palvelin_smtp[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x01;
        }
        /* Lähettäjän sähköposti */
        else if ((edellinen == '#') & (ReceivedByte == 'l')){ //pieni "L"
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)) {
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    sposti_lahettaja[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x02;
        }
        /* Lähettäjän salasana */
        else if ((edellinen == '#') & (ReceivedByte == 'S')){
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    salasana_lahettaja[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x04;
        }
        /* Lähettäjän nimi */
        else if ((edellinen == '#') & (ReceivedByte == 'L')){
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)) {
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    nimi_lahettaja[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x08;
        }
        /* Vastaanottajan sähköposti */
        else if ((edellinen == '#') & (ReceivedByte == 'v')){ 
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    sposti_vastaanottaja[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x10; 
        }
        /* Vastaanottajan nimi 
        else if ((edellinen == '#') & (ReceivedByte == 'V')){ 
            i = 0;
            while ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)) {
                ReceivedByte = uart_getbyte();
                if ((ReceivedByte != ' ')&&(ReceivedByte != '#')&&(ReceivedByte != 0x00)&&(ReceivedByte != 0x0D)){
                    nimi_vastaanottaja[i] = ReceivedByte;
                }
                ++i;
            }
            tallenna_eeprom_email |= 0x20; 
        }*/
        
        /* Tekstiviestilähetys päälle (tallenna_eeprom 0001 0000) */
        else if ((edellinen == '#') & (ReceivedByte == 'T')){
            haluttu_lahetystapa |= 0x01;
            tallenna_eeprom |= 0x10;
        }
        /* Tekstiviestilähetys pois  */
        else if ((edellinen == '#') & (ReceivedByte == 't')){
            haluttu_lahetystapa &= ~0x01;
            tallenna_eeprom |= 0x10;
        }
        /* Sähköpostilähetys päälle (tallenna_eeprom 0010 0000) */
        else if ((edellinen == '#') & (ReceivedByte == 'E')){
            haluttu_lahetystapa |= 0x02;
            tallenna_eeprom |= 0x20;
        }
        /* Sähköpostilähetys pois  */
        else if ((edellinen == '#') & (ReceivedByte == 'e')){
            haluttu_lahetystapa &= ~0x02;
            tallenna_eeprom |= 0x20;
        }
        /* yksinkertaisen kenttäpyynnön lukeminen. Näyttää ainoastaan rssi-osan (AT-manual s.94) */
        else if ((edellinen == '#') & (ReceivedByte == 'k')){
            kysy_kentta_saldo_debug |= 0x04;
        }
        /* saldokysely */
        else if ((edellinen == '#') & (ReceivedByte == 's')){
            kysy_kentta_saldo_debug |= 0x02;
        }
        /* debugviestin pyyntö */
        else if ((edellinen == '#') & (ReceivedByte == 'd')){
            kysy_kentta_saldo_debug |= 0x05;           
        }
        
        else if ((edellinen == 'O') & (ReceivedByte == 'K')){
            ++j;   
        }
        else if ((j == 1) & (ReceivedByte == 0x0D)){
            ++j;   
        }
        else if ((j == 2) & (ReceivedByte == 0x0A)){
            ++j;   
        }
        else if ((nytMillis - alkuMillis) > 20000){
            //sendSMS("Vaaka 1\r\nGSM-virhe: sms luku");
            break;                
        }
        edellinen = ReceivedByte;
    }
}

void poimi_kellonaika(void){    
  
    char edellinen = 0x00;
    unsigned long alkuMillis = millis();
    j = 0;
    while(j < 3){
        unsigned long nytMillis = millis();
        ReceivedByte = uart_getbyte();
        if (ReceivedByte == ','){
            for (i=0; i<2; i++){
                ReceivedByte = uart_getbyte();
                verkon_tunnit[i] = ReceivedByte; 
            }
            ReceivedByte = uart_getbyte();
            for (i=0; i<2; i++){
                ReceivedByte = uart_getbyte();                
                verkon_minuutit[i] = ReceivedByte;
            }           
        }
        else if ((edellinen == 'O') & (ReceivedByte == 'K')){
            ++j;   
        }
        else if ((j == 1) & (ReceivedByte == 0x0D)){
            ++j;   
        }
        else if ((j == 2) & (ReceivedByte == 0x0A)){
            ++j;   
        }
        else if ((nytMillis - alkuMillis) > 3000){
            break;                
        }
        edellinen = ReceivedByte;
    }
}

uint8_t rs232_init(){
    uint8_t virhe = 0;    
    char edellinen = 0x00;
    unsigned long alkuMillis = millis();
    uint8_t rs232_ok = 0;
    while (rs232_ok < 1){
        unsigned long nytMillis = millis();
        if(swSerial.available()){
            ReceivedByte = swSerial.read();
            if ((edellinen == 'G') & (ReceivedByte == 'S')){
                ++rs232_ok;
            }
        }
        else if (((nytMillis - alkuMillis) > 2000) & (virhe < 1)){ //suoritetaan jos vaaka ei ollut päällä valmiiksi
            PORTC |= (1 << PC4);        // toggle vaakavirtanappi: päälle
            _delay_ms(100);
            PORTC &= ~(1 << PC4);
            virhe = 1;
            alkuMillis = millis();
        }
        else if (((nytMillis - alkuMillis) > 15000) & (virhe < 2)){
            PORTC |= (1 << PC4);        // toggle vaakavirtanappi
            _delay_ms(100);
            PORTC &= ~(1 << PC4);
            virhe = 2;
        }
         else if ((nytMillis - alkuMillis) > 30000){
            //sendSMS("Vaaka 1\r\nVirhe: Painotietoja ei saatu.");
            return rs232_ok;                
        }
        edellinen = ReceivedByte;
    }
    if (rs232_ok == 1){
        alkuMillis = millis();
        // Vaa'an lähettää ainakin ensimmäisenä(ehkä toisenakin?) lukeman 0.00kg vaikka päällä olisi painoa.
        while ((ReceivedByte != '1')&&(ReceivedByte != '2')&&(ReceivedByte != '3')&&(ReceivedByte != '4')&&(ReceivedByte != '5')&&(ReceivedByte != '6')&&(ReceivedByte != '7')&&(ReceivedByte != '8')&&(ReceivedByte != '9')){  //||(ReceivedByte == 0x0A)|(ReceivedByte == '0')|(ReceivedByte == '.')|(ReceivedByte == ',')|(ReceivedByte == 0x20)|(ReceivedByte == 'S')|(ReceivedByte == 'T')|(ReceivedByte == 'G')|(ReceivedByte == 'k')|(ReceivedByte == 'g')){                
            unsigned long nytMillis = millis();
            if (swSerial.available()){
                ReceivedByte = swSerial.read();   
            }
            else if ((nytMillis - alkuMillis) > 1000){  //jos ei saada sekunnissa lukua 1-9 sarjapostista: paino on oikeasti 0. 
               
                break;                
            }
                         
        }
        while (ReceivedByte != 0x0A){                
            if (swSerial.available()){
                ReceivedByte = swSerial.read();   
            }
        }
        
    }
    return rs232_ok;
    
}

long readVcc() {

    // set the reference to Vcc and the measurement to the internal 1.1V reference      
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);    
    _delay_ms(2); // Wait for Vref to settle
    
    // Read 1.1V reference against AVcc
    ADCSRA |= _BV(ADSC); // Start conversion
    while (bit_is_set(ADCSRA,ADSC)); // measuring
    
    uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
    uint8_t high = ADCH; // unlocks both
    
    long result = (high<<8) | low;
    // Kalibrointi: mitataan Vcc tällä kontrollerin funktiolla ja sähkömittarilla.
    // calibrated_constant = (1.1*Vcc1[sähkömittarilla]/Vcc2[readVcc() funktiolla])*1023*1000 //ei kyllä ole ihan tarkka
    // parempi kalibroida mutulla. Calibrated_constant ja luettu Vcc ovat suunnilleen suoraan verrannolliset
    //  
    // Vaaka 01: 1105000L
    // Vaaka 02: 1130000L
    // Vaaka 03: 1145000L
    // Vaaka 04: 1132000L
    // Vaaka 05: 1105000L
    // vaaka 06: 1090000L
    long calibrated_constant = 1105000L; //>>>>>>>>>>>>>>>>>>>MUISTA KALIBROIDA<<<<<<<<<<<<<<<<
    result = calibrated_constant / result; // Calculate Vcc (in mV); 
    return result; // Vcc in millivolts
}

void watchdogOn() {
  
    // Clear the reset flag, the WDRF bit (bit 3) of MCUSR.
    MCUSR = MCUSR & B11110111;
      
    // Set the WDCE bit (bit 4) and the WDE bit (bit 3) 
    // of WDTCSR. The WDCE bit must be set in order to 
    // change WDE or the watchdog prescalers. Setting the 
    // WDCE bit will allow updtaes to the prescalers and 
    // WDE for 4 clock cycles then it will be reset by 
    // hardware.
    WDTCSR = WDTCSR | B00011000; 
    
    // Set the watchdog timeout prescaler value to 1024 K 
    // which will yeild a time-out interval of about 8.0 s.
    WDTCSR = B00100001;   //8.0s
    //WDTCSR = B00000110;     //1.0s

    
    // Enable the watchdog timer interupt.
    WDTCSR = WDTCSR | B01000000;
    MCUSR = MCUSR & B11110111;
}
    
void goToSleep(){
    set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Set sleep mode.
    adcsra = ADCSRA;               //save the ADC Control and Status Register A
    ADCSRA = 0;                    //disable ADC
    sleep_enable(); // Enable sleep mode.
    sleep_mode(); // Enter sleep mode.
    // After waking from watchdog interrupt the code continues
    // to execute from this point.
    
    sleep_disable(); // Disable sleep mode after waking.
    ADCSRA = adcsra; // Uudelleenkirjoita ADC Control and Status Register A
}

ISR(WDT_vect){
    sleep_count ++; // keep track of how many sleep cycles
    // have been completed.
}

int pyorista(int luku){
    int tasa = luku / 10;
    uint8_t jaannos = luku - 10 * (tasa);
    if (jaannos > 4){
        tasa ++;
    }
    return tasa;
}

void poimi_saldo(void){
    char edellinen = 0x00;
    unsigned long alkuMillis = millis();
    j = 0;
    while(j < 3){
        unsigned long nytMillis = millis();
        ReceivedByte = uart_getbyte();
        /* Puhelinnumeroviestin lukeminen */
        if ((edellinen == 'o') & (ReceivedByte == 'n')){
            for (i=0; i<6; i++) {
                ReceivedByte = uart_getbyte();
                saldo[i] = ReceivedByte;       
            }
        }
        else if ((edellinen == 'y') & (ReceivedByte == 'y')){
            for (i=0; i<11; i++){
                ReceivedByte = uart_getbyte();
                voim_oloaika[i] = ReceivedByte;
            }          
        }
        else if ((edellinen == 'O') & (ReceivedByte == 'K')){
            ++j;   
        }
        else if ((j == 1) & (ReceivedByte == 0x0D)){
            ++j;   
        }
        else if ((j == 2) & (ReceivedByte == 0x0A)){
            ++j;   
        }
        else if ((nytMillis - alkuMillis) > 20000){
            break;                
        }
        edellinen = ReceivedByte;
    }
}

void tilaa_saldotiedot(){
    sendAT("AT+CMGS=\"");
    sendAT("18258");  //vastaanottajan puh nro
    sendAT("\"\r\n");
    
    while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
    while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer

    sendAT("TILI");

    sendAT("\x1A\r\n");  // sends ctrl+z end of message      
    confirmAT();
    _delay_ms(30000); //odota 30sek. viestien saapumista                       
    
    sendAT("AT+CMGL=\"ALL\"\r\n");  //listaa viestit simkortilta
    poimi_saldo();
    sendAT("AT+CMGDA=DEL ALL\r\n"); //tyhjätään simkortilta viestit
    confirmAT();
}

void sendSMS(char buffer[]){
    sendAT("AT+CMGS=\"");
    sendAT(puh_vastaanottaja);
    sendAT("\"\r\n");
    
    while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
    while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer

    sendAT(buffer);
   
    sendAT("\x1A\r\n");  // sends ctrl+z end of message     
    confirmAT();
}

//*****************************************************************************************************
//*                                             SETUP                                                 *
//*****************************************************************************************************
void setup(){ 
    
    watchdogOn();
    
    //N-MOSFETIN ohjauspinni
    DDRD |= (1 << PD4);    //PD4 outputiksi
    PORTD &= ~(1 << PD4);  //alustetaan 0 N-MOSFET ei johda
   
    DDRC |= (1 << PC4);    //PC4 outputiksi
    PORTC &= ~(1 << PC4);  //alustetaan 0 jolloin opto-transistori OFF

    DDRD |= (1 << PD1);    // UART TX = output
    DDRD &= ~(1 << PD0);   // UART RX = input

    // Haluttu baud rate: 9600 baud/s
    // UBRR0 = (Fcpu / (16*baud rate))-1
      
    UBRR0H = 0x00;
    //UBRR0L = 0x67; // @ 16 MHz
    UBRR0L = 0x33; // @ 8 MHz
    //UBRR0L = 0x17; // @ 3,6864 Mhz
    
    // Enable UART0 transmit, receive and receive interrupts
    //UCSR0B |= (1 << TXEN0) | (1 << RXEN0) | (1 << RXCIE0);    
    UCSR0B |= (1 << TXEN0) | (1 << RXEN0);
    
    _delay_ms(50);

    swSerial.begin(9600);  //Open swSerial connection to RS232 at 9600 baud
    

    /****************Luetaan EEPROMiin tallennetut tiedot*****************/
    
    //eeprom_read_block((void*)laitteen_nimi, (const void*)EEnimi, 9);
    //eeprom_read_block((void*)puh_admin, (const void*)EEadmin_puh, 11);
    eeprom_read_block((void*)puh_vastaanottaja, (const void*)EEpuh, 11);
    //eeprom_read_block((void*)palvelin_smtp, (const void*)EEsmtp_palvelin, 31);
    //eeprom_read_block((void*)sposti_lahettaja, (const void*)EElahettaja_sposti, 31);
    //eeprom_read_block((void*)salasana_lahettaja, (const void*)EElahettaja_salasana, 31);
    //eeprom_read_block((void*)nimi_lahettaja, (const void*)EElahettaja_nimi, 31);
    eeprom_read_block((void*)sposti_vastaanottaja, (const void*)EEvastaanottaja_sposti, 31);
    //eeprom_read_block((void*)nimi_vastaanottaja, (const void*)EEvastaanottaja_nimi, 31);
    haluttu_aika = eeprom_read_word(&EEaika);
    haluttu_lahetystapa = eeprom_read_word(&EElahetystapa);  
}

//*****************************************************************************************************
//*                                             MAIN                                                  *
//*****************************************************************************************************
void loop(){
/************************************ Käyttöjännitteen mittaaminen ******************************/
        _delay_ms(100);        // Odota Vcc tasautumista
        
        V_patterit = readVcc();
        uint8_t V_patterit_tasa = V_patterit / 1000;
        uint16_t V_patterit_decim = V_patterit - 1000 * (V_patterit_tasa);
        V_patterit_decim /= 10;
        char kayttojannite_tasa[2];
        char kayttojannite_decim[3];
        sprintf(kayttojannite_tasa, "%u", V_patterit_tasa);
        sprintf(kayttojannite_decim, "%02u", V_patterit_decim); //lisätään nollia

/************************************ GSM initialization ************************************/   
        PORTD |= (1 << PD4);        //MOSFET päälle
        _delay_ms(20);  //odota mosfetin kytkeytyminen
        
        unsigned long alkuMillis = millis();
        uint8_t gsm_init = 0;
        char edellinen = 0x00;
        uint8_t virhe = 0;
        while(gsm_init < 5){ // Wait for GSM to send SMS Ready, Call Ready yms network conn. messages            
            unsigned long nytMillis = millis();
                        
            ReceivedByte = uart_getbyte();            
            if ((edellinen == 'd') & (ReceivedByte == 'y')){ //tulee kahteen kertaan!! siksi j<5!
                ++gsm_init;   
            }
            else if ((edellinen == 'E') & (ReceivedByte == 'V')){
                ++gsm_init;   
            }
            else if ((gsm_init == 3) & (ReceivedByte == 0x0D)){
                ++gsm_init;   
            }
            else if ((gsm_init == 4) & (ReceivedByte == 0x0A)){
                ++gsm_init;   
            }
            else if (((nytMillis - alkuMillis) > 30000) & (virhe == 0)){
                //sendAT("GSM-alustusvirhe: Ei verkkoa. Uudelleenkäynnistetään GSM...\r\n");
                PORTD &= ~(1 << PD4);       // MOSFET pois päältä;
                _delay_ms(1000);
                PORTD |= (1 << PD4);        //MOSFET päälle
                _delay_ms(20);
                gsm_init = 0;
                virhe = 1;
                alkuMillis = millis();      //aloitetaan ajan laskenta alusta          
            }
            else if ((nytMillis - alkuMillis) > 40000){ //31 sek jottei molemmat eliffit ole 30sek
                //sendAT("GSM-alustusvirhe: Kenttää ei havaittu. Ohitetaan...\r\n");
                break;
            }
            edellinen = ReceivedByte;
        }
        // Jos GSM-alustus onnistuu, siirrytään suorittamaan tominnot.
        // Muuten suoraan mosfet pois ja nukkumaan uni_minuuteissa-ajaksi.
        if(gsm_init == 5){   //tästä alkaa päätoiminnot sisältävä if.
          
/************************** Asetusviestin lukeminen & EEPROM-tallennukset ************************************/
            _delay_ms(30000); //odota 30sek. viestien saapumista                       
    
            sendAT("AT+CMGL=\"ALL\"\r\n");  //listaa viestit simkortilta
            poimi_komennot_viesteista();                
            sendAT("AT+CMGDA=DEL ALL\r\n"); //tyhjätään simkortilta viestit
            confirmAT();
            
            //Tallennetaan kyseinen tieto EEPROMiin jos saatu viestinä nimi, puh, tai aika
            /*
            if (tallenna_eeprom & 0x01){ //jos bitti on 1
                eeprom_update_block((const void*)laitteen_nimi, (void*)EEnimi, 9);
                _delay_ms(100);
                tallenna_eeprom &= ~0x01;   //nollataan bitti             
            }
            
            if (tallenna_eeprom & 0x02){
                eeprom_update_block((const void*)puh_admin, (void*)EEadmin_puh, 11);
                _delay_ms(100);
                tallenna_eeprom &= ~0x08;   //nollataan bitti
            }
            */
            if (tallenna_eeprom & 0x02){
                eeprom_update_block((const void*)puh_vastaanottaja, (void*)EEpuh, 11);
                _delay_ms(100);
                tallenna_eeprom &= ~0x02;   //nollataan bitti
            }
            if (tallenna_eeprom & 0x04){
                eeprom_update_word(&EEaika, haluttu_aika);
                _delay_ms(100);
                tallenna_eeprom &= ~0x04;   //nollataan bitti
            }
            /*
            if (tallenna_eeprom_email & 0x01){
                
                eeprom_update_block((const void*)clear_eeprom, (void*)EEsmtp_palvelin, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)palvelin_smtp, (void*)EEsmtp_palvelin, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x01;   //nollataan bitti
            }
            if (tallenna_eeprom_email & 0x02){
                
                eeprom_update_block((const void*)clear_eeprom, (void*)EElahettaja_sposti, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)sposti_lahettaja, (void*)EElahettaja_sposti, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x02;   //nollataan bitti
            }
            if (tallenna_eeprom_email & 0x04){
                eeprom_update_block((const void*)clear_eeprom, (void*)EElahettaja_salasana, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)salasana_lahettaja, (void*)EElahettaja_salasana, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x04;   //nollataan bitti
            }
            if (tallenna_eeprom_email & 0x08){
                eeprom_update_block((const void*)clear_eeprom, (void*)EElahettaja_nimi, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)nimi_lahettaja, (void*)EElahettaja_nimi, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x08;   //nollataan bitti
            }
            */
            if (tallenna_eeprom_email & 0x10){
                eeprom_update_block((const void*)clear_eeprom, (void*)EEvastaanottaja_sposti, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)sposti_vastaanottaja, (void*)EEvastaanottaja_sposti, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x10;   //nollataan bitti
            }
            /*if (tallenna_eeprom_email & 0x20){
                eeprom_update_block((const void*)clear_eeprom, (void*)EEvastaanottaja_nimi, 31);
                _delay_ms(2000);
                eeprom_update_block((const void*)nimi_vastaanottaja, (void*)EEvastaanottaja_nimi, 31);
                _delay_ms(100);
                tallenna_eeprom_email &= ~0x20;   //nollataan bitti
            }*/
            if (tallenna_eeprom & 0x10){ //tekstiviesti
                eeprom_update_word(&EElahetystapa, haluttu_lahetystapa);
                _delay_ms(100);
                tallenna_eeprom &= ~0x10;
            }
            if (tallenna_eeprom & 0x20){ //email
                eeprom_update_word(&EElahetystapa, haluttu_lahetystapa);
                _delay_ms(100);
                tallenna_eeprom &= ~0x20;
            }
            
            /**************** KENTTÄKYSELY**********************/
            if (kysy_kentta_saldo_debug & 0x04){ //jos viesteistä luettu #k
                
                char edellinen = 0x00;                
                unsigned long alkuMillis = millis();
                sendAT("AT+CSQ\r\n");
                j = 0;
                while(j < 3){
                    unsigned long nytMillis = millis();
                    ReceivedByte = uart_getbyte();
                    if (ReceivedByte == ' '){
                        for (i=0; i<5; i++){
                            ReceivedByte = uart_getbyte();
                            //kenttatiedot[i] = ReceivedByte; //ei tallenneta enää laajaa kenttää
                            if (i < 2){
                                 kentta[i] = ReceivedByte;
                            }
                        }          
                    }
                    else if ((edellinen == 'O') & (ReceivedByte == 'K')){
                        ++j;   
                    }
                    else if ((j == 1) & (ReceivedByte == 0x0D)){
                        ++j;   
                    }
                    else if ((j == 2) & (ReceivedByte == 0x0A)){
                        ++j;   
                    }
                    else if ((nytMillis - alkuMillis) > 3000){
                        sendSMS("Vaaka 1\r\nGSM-virhe: signaalikysely");
                        break;
                    }
                    edellinen = ReceivedByte;
                }
            }
            if (kysy_kentta_saldo_debug & 0x02){ //jos viesteistä luettu #s
                tilaa_saldotiedot();                
            }
    
    /*********************************** Vaa'an akun jännitteen lukeminen ************************************/
            
            for (i=0; i< NUMSAMPLES; i++) {
                samples[i] = analogRead(A2);            
                _delay_ms(2);
            }
            // average all the samples out
            average = 0;
            for (i=0; i < NUMSAMPLES; i++) {
                average += samples[i];
            }
            average /= NUMSAMPLES;
            int jannite = average * (V_patterit/1023.0); //pitää olla desimaalipiste!?
            float V_akku = ((R1 + R2)/(R2)) * jannite;
            uint8_t V_akku_tasa = V_akku / 1000;
            uint8_t V_akku_decim = V_akku - 1000 * (V_akku_tasa); //jakojäännös
            V_akku_decim /= 10;
            
            char akun_jannite_tasa[2];
            char akun_jannite_decim[3];        
            sprintf(akun_jannite_tasa, "%u", V_akku_tasa);
            sprintf(akun_jannite_decim, "%02u", V_akku_decim); //lisätään nollia
            
            _delay_ms(10);
           
    /**************************************** Lämpötilan lukeminen *********************************/
            // take N samples in a row, with a slight _delay_ms
            for (i=0; i < NUMSAMPLES; i++) {
                samples[i] = analogRead(THERMISTORPIN);
                _delay_ms(2);
            }
            // average all the samples out
            average = 0;
            for (i=0; i< NUMSAMPLES; i++) {
                average += samples[i];
            }
            average /= NUMSAMPLES;
        
            // convert the value to resistance
            average = 1023 / average - 1;
            average = SERIESRESISTOR / average;
                 
            float steinhart;
            steinhart = average / THERMISTORNOMINAL;     // (R/Ro)
            steinhart = log(steinhart);                  // ln(R/Ro)
            steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
            steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
            steinhart = 1.0 / steinhart;                 // Invert
            steinhart -= 273.15;                         // convert to C
            steinhart = round(round(steinhart));
            char lampotila[3];                    
            itoa(steinhart, lampotila, 10);
    
    
    /******************************** Painotietojen käsittely ********************************/  
     
            char painotiedot[18] = "XX,XX   --,-- kg";
                                
            // Vaaka hoidetaan päälle rs232_init-funktiossa!
            if (rs232_init() == 1){
                for (i=0; i<10; i++) {           //lukee sarjaporttia pilkkuun asti (0.00,kg)
                    while(!swSerial.available());
                    ReceivedByte = swSerial.read();
                    painotiedot[i] = ReceivedByte;               
                }
                while(!swSerial.available());
                ReceivedByte = swSerial.read(); //skipataan desimaalipiste
                painotiedot[10] = ',';
                for (i=11; i<13; i++) {           //lukee sarjaporttia pilkkuun asti (0.00,kg)
                    while(!swSerial.available());
                    ReceivedByte = swSerial.read();
                    painotiedot[i] = ReceivedByte;               
                }
                PORTC |= (1 << PC4);        // toggle vaakavirtanappi: pois
                _delay_ms(100);
                PORTC &= ~(1 << PC4);
            }
            

    /************************************ Tekstiviestin lähetys ***********************************/
            
            
            if (kysy_kentta_saldo_debug & 0x01){ //debugviestin iffi ensin ja jos ei toteudu niin sitte normiviesti
                sendAT("AT+CMGS=\"");
                sendAT(puh_admin);
                sendAT("\"\r\n");
                
                while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
                while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer
                
                sendAT(laitteen_nimi);              // LAITTEEN NIMI
                sendAT(", ver:");
                sendAT(versio);
                sendAT(", s/n:");
                sendAT(sarjanro);
                sendAT(", fcpu:");
                sendAT(fcpu);
                sendAT("\r\n");
                sendAT("P:");
                sendAT(painotiedot);
                sendAT(" L:");
                sendAT(lampotila);  
                sendAT("C");
                sendAT("\r\nVcc:");
                sendAT(kayttojannite_tasa);
                sendAT(",");
                sendAT(kayttojannite_decim);
                sendAT("V");
                sendAT(" Pb:");
                sendAT(akun_jannite_tasa);
                sendAT(",");
                sendAT(akun_jannite_decim);
                sendAT("V\r\n");
                //char lkm[5];
                //itoa(looppien_lkm, lkm, 10);
                //sendAT("Kierros:");
                //sendAT(lkm);
                sendAT("Asetettu_a:");
                char aika[5];
                itoa(haluttu_aika, aika, 10);
                sendAT(aika);
                char last_interval[5];
                itoa(uni_minuuteissa,last_interval, 10);
                sendAT(" viim_unijakso:");
                sendAT(last_interval);
                sendAT(" K:");
                sendAT(kentta);
                sendAT("/31");
                sendAT("\r\nL_tapa: ");
                char asetettu_lahetystapa[9];
                sprintf(asetettu_lahetystapa, "%x", haluttu_lahetystapa);
                sendAT(asetettu_lahetystapa);
                sendAT("\r\nEmail asetukset: ");
                sendAT("\r\n");
                sendAT("P:");
                sendAT(palvelin_smtp);
                sendAT(" l:");
                sendAT(sposti_lahettaja);
                sendAT(" S:");
                sendAT(salasana_lahettaja);
                sendAT(" L:");
                sendAT(nimi_lahettaja);
                sendAT(" v:");
                sendAT(sposti_vastaanottaja);
                //sendAT(" V:");
                //sendAT(nimi_vastaanottaja);

                kysy_kentta_saldo_debug &= ~0x05; //nollataan kenttäbitti ja debugbitti 0x05 = 0b0000 0101
            }
            if(haluttu_lahetystapa & 0x01){
              
                sendAT("AT+CMGS=\"");
                sendAT(puh_vastaanottaja);
                sendAT("\"\r\n");
                
                while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
                while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer
                
                sendAT(laitteen_nimi);              // LAITTEEN NIMI
                sendAT("\r\n");
                sendAT("Paino: ");
                sendAT(painotiedot);
                sendAT("\r\nL\xE4mp\xF6tila: "); 
                sendAT(lampotila);  
                sendAT(" C\r\n");
                sendAT("L\xE4hettimen akku: ");
                sendAT(kayttojannite_tasa);
                sendAT(",");
                sendAT(kayttojannite_decim);
                sendAT(" V\r\n");
                sendAT("Vaa'an akku: ");
                sendAT(akun_jannite_tasa);
                sendAT(",");
                sendAT(akun_jannite_decim);
                sendAT(" V\r\n");

                if (kysy_kentta_saldo_debug & 0x04){
                    sendAT("Kentt\xE4: ");
                    sendAT(kentta);
                    sendAT("/31\r\n");
                    kysy_kentta_saldo_debug &= ~0x04; //nollataan kenttäbitit 0x0C = 0b0000 1100
                }
                if (kysy_kentta_saldo_debug & 0x02){
                    sendAT("Saldo:");
                    sendAT(saldo);
                    sendAT("EUR voim.");
                    sendAT(voim_oloaika);
                    sendAT(" asti");
                    kysy_kentta_saldo_debug &= ~0x02;    
                }
                
            }
                        
            
            sendAT("\x1A\r\n");  // sends ctrl+z end of message     
            confirmAT();
            
                      
/********************************* Sähköpostiviestin lähetys *********************************/
        if(haluttu_lahetystapa & 0x02){
          
            sendAT("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n"); 
            confirmAT();
            sendAT("AT+SAPBR=3,1,\"APN\",\"internet\"\r\n");            //prepaid liittymä
            //sendAT("AT+SAPBR=3,1,\"APN\",\"internet.saunalahti\"\r\n"); //3G dataliittymä
            confirmAT();
            sendAT("AT+SAPBR=1,1\r\n");
            confirmAT();
            sendAT("at+emailcid=1\r\n");
            confirmAT();
            sendAT("at+emailto=60\r\n");
            confirmAT();
            sendAT("at+emailssl=1\r\n");
            confirmAT();
            sendAT("at+smtpsrv=\"");
            sendAT(palvelin_smtp);
            sendAT("\"\r\n"); //SSL-tuetussa moduulissa ei tarvi porttia mainita erikseen.
            //sendAT("\",2525\r\n"); //2525 on smtp2go:n suosittelema portti
            confirmAT();
            sendAT("at+smtpauth=1,\"");
            sendAT(sposti_lahettaja);
            sendAT("\",\"");
            sendAT(salasana_lahettaja);
            sendAT("\"\r\n");
            confirmAT();
            sendAT("at+smtpfrom=\"");
            sendAT(sposti_lahettaja);
            sendAT("\",\"");
            sendAT(nimi_lahettaja);
            sendAT("\"\r\n");
            confirmAT();
            sendAT("at+smtpsub=\"");
            sendAT(laitteen_nimi);
            //sendAT("5661616B61203032"); //"Vaaka 02"
            sendAT("\"\r\n");
            confirmAT();
            sendAT("at+smtprcpt=0,0,\"");
            sendAT(sposti_vastaanottaja);
            sendAT("\",\"");
            //sendAT(nimi_vastaanottaja);
            sendAT(sposti_vastaanottaja);
            sendAT("\"\r\n");
            confirmAT();
            sendAT("at+smtpbody=");
            sendAT("82");  //VIESTIN PITUUS
            sendAT("\r\n");
            while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
            while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer
            
            //Teksti muunnettu heksadesimaalimuotoon koska UTF-8 sähköpostissa
            //sendAT_hex(laitteen_nimi);              // LAITTEEN NIMI
            sendAT("Paino: ");         //"\r\nPaino: "
            sendAT(painotiedot);
            sendAT("\r\nAsteita: ");  //"\r\nL\xE4mp\xF6tila: "
            sendAT(lampotila);  
              
            sendAT(" C\r\nEneloopit: ");     //" °C\r\nL\xE4hettimen akku: "
            sendAT(kayttojannite_tasa);
            sendAT(",");                        //pilkku
            sendAT(kayttojannite_decim);
            sendAT(" V\r\nVaa'an akku: ");      //" V\r\nVaa'an akku: "
            sendAT(akun_jannite_tasa);
            sendAT(",");                        //pilkku
            sendAT(akun_jannite_decim);
            sendAT(" V        ");                //" V\r\n"
            confirmAT();
                   
            sendAT("at+smtpsend\r\n");
            confirmAT();
            confirmEMAIL(); //odottaa plussaa
            
            //while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
            //while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer
            sendAT("at+sapbr=0,1\r\n");
            confirmAT();
            //while(!(UCSR0A & (1<<RXC0)));        // Wait until a data is available
            //while(!(UCSR0A & (1<<UDRE0)));       // Wait for empty transmit buffer
        }         
                    
/******************* Kysytään verkon kellonaika ja luodaan tiedosta nukkumisintervalli *******************/   
            sendAT("AT+CCLK?\r\n");
            poimi_kellonaika();  
            sendAT("AT+CPOWD=1\r\n");   // Normal power down.
            confirmPOWD();    
            
                
        }  //tähän päättyy päätoiminnot sisältävä "if".           



        PORTD &= ~(1 << PD4);       // MOSFET pois päältä

        //++looppien_lkm; // lasketaan montako pääohjelman kierrosta on suoritettu.

        verkon_aika = ((atoi(verkon_tunnit))*60) + atoi(verkon_minuutit);               
        uni_minuuteissa = edellinen_uni_minuuteissa + haluttu_aika - verkon_aika;        
        if (uni_minuuteissa > 1499){
            uni_minuuteissa -= 1439;            
        }

        edellinen_uni_minuuteissa = uni_minuuteissa;
/**************************************** SLEEP ***************************************************/
/* Kun fighfuset on 0xFF, power_downista heräämiseen on varattu 16k kellojaksoa + 65ms! eli unijakson pituus
 * on noin 8s + 16k CK + 65ms = 8s + 0,001s + 0,065s
 * kuitenkin kuuden tunnin "real world" intervallista (viestien välisestä ajasta miinus minuutin kestävä
 * ohjelman suoritus) mitattuna 8,2888 sek.
 * Jos lähetin aloittaa pääohjelman ennen kuin on vuorokausi kulunut, se laskee nukkumisintervallisksi 
 * jäljellä olevan ajan joten viestejä tulee turhaan kaksi putkeen. näin kävi kun unijakson ajaksi asetettu
 * 8,28 sek. Tällöin nukkumislooppi noin 5 min liian lyhyt. 
 */
        
        sleep_total = ((uni_minuuteissa*600.0)/82.7);    // kerrottu sekuntit(60s) ja unijakson pituus(8,3s) kymmenellä, jotta vältytään floateilta

        sleep_count = 0;     
        while (sleep_count < sleep_total){  // If > and not == sleep lasts 1 cycle (8sec) more! >>>>>>>HUOM HUOM<<<<<<<<
            goToSleep(); // ATmega328 goes to sleep for about X seconds (ks. WDT-alustusfunktio)
        }
        sleep_count = 0;
}
