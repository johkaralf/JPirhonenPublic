// Module: query_functions / file: query_functions.cpp
// Source code for the needed functions and algorithms for a nested map structure of type
// map<string, map<string, map<string, double>>>:
// Includes the implementations for printing each map level individually and a search algorithm that finds and prints
// the smallest value (double) of the provided map3 (inner maps) key in the whole data structure, including parallel outer level maps,
// and prints all the paths in the tree, leading to this found value.
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <string>

using namespace std;

// Private iterators to be used only by this module.
namespace {
    map<string, map<string, map<string, double>>>::iterator map1_iter;
    map<string, map<string, double>>::iterator map2_iter;
    map<string, double>::iterator map3_iter;
}

// This function prints all keys of the outermost map (map1) in the data structure and returns a boolean value true.
// If the map structure input is empty, the function returns false.
bool print_keys_map1(map<string, map<string, map<string, double>>>& tree) {
    if (!tree.empty()) {
        map1_iter = tree.begin();
        while (map1_iter != tree.end()) {
            cout << map1_iter->first << endl;
            ++map1_iter;
        }
        return true;

    } else {
        cout << "Error: map tree is empty!" << endl;
        return false;
    }
}

// This function frints all keys of the center map (map2). Input parameters are a nested map structure as seen below, and
// a valid key from the outermost map (map1). The function prints map2 and returns true, if the map1 input key is found.
// other wise false is returned.
bool print_keys_map2(map<string, map<string, map<string, double>>>& tree, const string& map1_key){
    map1_iter = tree.find(map1_key);


    if (map1_iter != tree.end()) {
        // Map1 key found.
        map<string, map<string, double>>& map2 = map1_iter->second;
        map2_iter = map2.begin();

        while (map2_iter != map2.end()) {
            cout << map2_iter->first << endl;
            ++map2_iter;
        }
        return true;
    } else {        
        return false;
    }
}

// Finds the smallest value (double) of the provided map3 key in the whole data structure, including parallel outer level maps, and
// prints all the paths in the tree, leading to this found value. In this case it finds where a certaind product is the
// cheapest and prints the price and all the shops where the product can be found at this price. The function returns false if
// the input parameter key is not found in the inner map (map3). Otherwise the function creates a temporary map to store the
// lowest value found (as a key) and all the paths to the keys (of map3) of this value in the tree (as one string). Then this
// temporary map is printed, and true is returned.
bool print_smallest_element_paths_tree(map<string, map<string, map<string, double>>>& tree, const string& key){

    map<double, string> found_value_paths;

    map1_iter = tree.begin();

    while (map1_iter != tree.end()) {
        map2_iter = map1_iter->second.begin();

        while (map2_iter !=map1_iter->second.end()) {
            map3_iter = map2_iter->second.find(key);

            if (map3_iter != map2_iter->second.end()) {
                // Map3 key found. Insert path to double.
                found_value_paths[map3_iter->second] += map1_iter->first + " " + map2_iter->first + "\n";

            }
            ++map2_iter;

        }
        ++map1_iter;
    }
    if (found_value_paths.empty()) {
        // Map3 key not found.
        return false;

    } else {
        // Because of the automatically sorted map container, printing the first key/value pair
        // shows the lowest double and corresponding path string in the map. No sorting needed.
        cout << fixed << setprecision(2)
             << found_value_paths.begin()->first << endl
             << found_value_paths.begin()->second;
        return true;
    }
}

// This function takes as input parameters a data structure and keys from the outer map (map1) and center map (map2). If both
// keys are valid. The function prints the corresponding inner map (map3) and returns true.
bool print_map3(map<string, map<string, map<string, double>>>& tree, const string& map1_key, const string& map2_key){
    map1_iter = tree.find(map1_key);

    if (map1_iter != tree.end()) {
        // Map1 key found.
        map<string, map<string, double>>& map2 = map1_iter->second;
        map2_iter = map2.find(map2_key);

        if (map2_iter != map2.end()) {
            // Map2 key found
            map<string, double> map3 = map2_iter->second;
            map3_iter = map3.begin();

            while (map3_iter != map3.end()) {
                // Prints all key/value pairs in the innermost map (map3).
                // The doubles are printed with 2 decimal places.
                cout << fixed << setprecision(2)
                     << map3_iter->first << " " << map3_iter->second << endl;
                ++map3_iter;
            }
            return true;

        } else {
            //cout << "Error: map2_key \"" << map2_key << "\" not found!" << endl;
            return false;
        }

    } else {
        //cout << "Error: map1_key \"" << map1_key << "\" not found!" << endl;
        return false;
    }
}
