//--------------------------------------------------------------------------//
//                                                                          //
// TIE-02206 Basic Course on Programming, S2017                             //
//                                                                          //
// Programmer:  Johannes Pirhonen                                           //
// Student ID:  224613                                                      //
// E-mail:      johannes.pirhonen@student.tut.fi                            //
// Work:        project-02 (product search)                                 //
// Modified:    5.11.2017                                                   //
//                                                                          //
// This program is used to search information from a nested map data        //
// structure of type map<string, map<string, map<string, double>>>, based   //
// on a csv-file "products.txt". In this case the structure holds info      //
// about store chains, store locations, products in a store and the price   //
// of each product. The valid format of the csv without error checking is   //
// "store_chain;store_location;product_name;product_price". The program     //
// consists of 4 modules: csv-parser, query_functions, splitter and main.   //
// The first two consist of auxiliary functions to read and parse the       //
// information from the csv to the data stucture and to search and print    //
// infromation from the data structure. The splitter is a class for string  //
// splitting.                                                               //
//                                                                          //
// The user interface is implemented here in main.cpp. The UI is done using //
// a while-loop that reads keyboard input until the exiting command "quit"  //
// is typed. If the csv-file is not found in the working directory, an error//
// occurs and execution ends. If the file is successfully read, information //
// from the data structure can be obtained with commands outlined in the    //
// HELP_TEXT that can be displayed by typing --help.                        //
//                                                                          //
//--------------------------------------------------------------------------//
#include "csv_parser.hh"
#include "query_functions.hh"
#include "splitter.hh"
#include <iostream>
#include <map>
#include <string>

using namespace std;

// Text to display when --help is invoked.
const string HELP_TEXT = "\nSearch from a data structure based on "
                         "the provided csv-file \"products.txt\"\n\n"
                         "Valid commands:\n"
                         " chains                       - Chain names\n"
                         " stores [chain]               - Store locations of a chain\n"
                         " selection [chain] [location] - Products and product prices of a store\n"
                         " cheapest [product]           - Search for the cheapest price and where to buy\n"
                         " quit                         - Exit program";

// Error message to display when the expected input arguments of a command are not read.
const string ERROR_INPUT_ARGUMENTS = "Error: bad input arguments. Type --help for correct usage.";

int main() {

    string products_csv = "products.txt";
    map<string, map<string, map<string, double>>> catalog;

    // The function csv_to_map_tree returns true if the csv-file is successfully read and parsed.
    // If so, the command string is declared, splitter object creater and while loop for UI is entered.
    if (csv_to_map_tree(products_csv, catalog)) {
        string command;
        Splitter command_splitter(command);

        // UI loop that returns when command == quit.
        while (true) {
            cout << "product search> ";

            getline(cin, command);

            // User input is splitted into fields using space as a splitting char.
            // Fields are later accessed by index.
            command_splitter.set_string_to_split(command);
            command_splitter.split(' ');

            // Next the command is checked and appropriate actions are done.

            if (command == "quit") {
                return 0;

            } else if (command == "--help") {
                cout << HELP_TEXT << endl;

            } else if (command == "chains") {
                // Prints all chains in the data structure.
                print_keys_map1(catalog);

            } else if (command_splitter.fetch_field(0) == "stores") {
                // Here the correct amout of input arguments for the command "stores" is tested.
                if (command_splitter.number_of_fields() == 2) {
                    // Prints all stores of a chain if print_keys_map2 returns true i.e. chain exists.
                    if (!print_keys_map2(catalog, command_splitter.fetch_field(1))) {
                        cout << "Error: chain \"" << command_splitter.fetch_field(1) << "\" not found!" << endl;
                    }
                } else {
                    cout << ERROR_INPUT_ARGUMENTS << endl;
                }

            } else if (command_splitter.fetch_field(0) == "selection") {
                // Here the correct amout of input arguments for the command "selection" is tested.
                if (command_splitter.number_of_fields() == 3) {
                    // Prints all products and corresponding prices if print_map3 returns true i.e.
                    // chain and store exist.
                    if (!print_map3(catalog, command_splitter.fetch_field(1), command_splitter.fetch_field(2))) {
                            cout << "Error: store not found" << endl;
                    }
                } else {
                    cout << ERROR_INPUT_ARGUMENTS << endl;
                }

            } else if (command_splitter.fetch_field(0) == "cheapest") {
                // Here the correct amout of input arguments for the command "cheapest" is tested.
                if (command_splitter.number_of_fields() == 2) {
                    // The lowest price and available stores of the searched product are printed.
                    if (!print_smallest_element_paths_tree(catalog, command_splitter.fetch_field(1))) {
                        cout << "This product is not available anywhere." << endl;
                    }
                } else {
                    cout << ERROR_INPUT_ARGUMENTS << endl;
                }

            } else {
                cout << "Error: " << command <<": command not found. Type --help for available commands." << endl;
            }
        }

    } else {
        cout << "Error: the input file can not be read." << endl;
        return 0;
    }
}
