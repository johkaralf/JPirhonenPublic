// Module: csv_parser / file: csv_parser.hh
// The header file for the csv_parser module:
// holds the declaration for the function needed to read and parse the csv-file.
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#ifndef CSV_PARSER_HH
#define CSV_PARSER_HH

#include <map>
#include <string>

using namespace std;

// This function takes a nested map and a csv-file, of type store_chain;store_location;product_name;product_price,
// as input parameters. The function opens the indicated file, parses the fields separated by a semicolon,
// and adds each field to the map data structure. The function returns false if the file can not be read, or
// true if the file is read, the data is stored and the file closed successfully.
bool csv_to_map_tree(const string& input_file,
                     map<string, map<string, map<string, double>>>& tree);

#endif // CSV_PARSER_HH
