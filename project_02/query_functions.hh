// Module: query_functions / file: query_functions.hh
// Header file for quiery_functions module:
// contains declarations for functions that perform
// searches and prints out elements of nested map
// structures of 3 levels of type (map<string, map<string, map<string, double>>>).
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#ifndef QUERY_FUNCTIONS_HH
#define QUERY_FUNCTIONS_HH

#include <iostream>
#include <map>
#include <string>

using namespace std;

// This function prints all keys of the outermost map (map1) in the data structure and returns a boolean value true.
// If the map structure input is empty, the function returns false.
bool print_keys_map1(map<string, map<string, map<string, double>>>& tree);

// This function frints all keys of the center map (map2). Input parameters are a nested map structure as seen below, and
// a valid key from the outermost map (map1). The function prints map2 and returns true, if the map1 input key is found.
// other wise false is returned.
bool print_keys_map2(map<string, map<string, map<string, double>>>& tree, const string& map1_key);

// Finds the smallest value (double) of the provided map3 key in the whole data structure, including parallel outer levelmaps, and
// prints all the paths in the tree, leading to this found value. In this case it finds where a certaind product is the
// cheapest and prints the price and all the shops where the product can be found at this price. The function returns false if
// the input parameter key is not found in the inner map (map3). Otherwise the function creates a temporary map to store the
// lowest value found (as a key) and all the paths to the keys (of map3) of this value in the tree (as one string). Then this
// temporary map is printed, and true is returned.
bool print_smallest_element_paths_tree(map<string, map<string, map<string, double>>>& tree, const string& key);

// This function takes as input parameters a data structure and keys from the outer map (map1) and center map (map2). If both
// keys are valid. The function prints the corresponding inner map (map3) and returns true.
bool print_map3(map<string, map<string, map<string, double>>>& tree, const string& map1_key, const string& map2_key);

#endif // QUERY_FUNCTIONS_HH
