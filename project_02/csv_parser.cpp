// Module: csv_parser / file: csv_parser.cpp
// The source file containing definitions and functions for the csv_parser module:
// includes code to do the needed file operations, use the Splitter class and insert the
// rows from the input text file to the nested map data structure.
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#include "splitter.hh"
#include <fstream>
#include <iostream>
#include <map>
#include <string>

using namespace std;

// The private part of the csv_parser.
namespace {

    // This function needs the nested map sturcture and all the fields from the csv-file "products.txt"
    // as input parameters. The function is used only in this module and the fields are taken
    // from the splitter object that contains the information of a row from the csv-file. The function
    // has no return value.
    void add_product(map<string, map<string, map<string, double>>>& product_catalog,
                     const string& store_chain,
                     const string& store_location,
                     const string& product_name,
                     const double product_price) {

        // The map structure is iterated through with an iterator for each map. The location of the previous iterator
        // is assigned to the next and this again to the last iterator.
        map<string, map<string, map<string, double>>>::iterator chain_iter;
        chain_iter = product_catalog.find(store_chain);

        if ( chain_iter == product_catalog.end() ) {
            // STORE CHAIN NOT FOUND
            // The new store chain, store location, product and price are inserted to the map structure.
            product_catalog[store_chain][store_location][product_name] = product_price;

        } else {
            // STORE CHAIN FOUND
            map<string, map<string, double>>::iterator location_iter;
            location_iter = chain_iter->second.find(store_location);
            if ( location_iter == chain_iter->second.end() ) {
                // STORE LOCATION NOT FOUND
                // The new store location, product and price are inserted to the map structure.
                chain_iter->second[store_location][product_name] = product_price;

            } else {

                // STORE LOCATION FOUND
                map<string, double>::iterator product_iter;
                product_iter = location_iter->second.find(product_name);
                if ( product_iter == location_iter->second.end() ) {
                    // PRODUCT NOT FOUND
                    // The new product and price are inserted to the map structure.
                    location_iter->second.insert({product_name, product_price});
                } else {
                    // PRODUCT FOUND
                    // In the case of the product already existing in a certain store, the price is updated.
                    product_iter->second = product_price;
                }
            }
        }
    }
}

// This function takes a nested map and a csv-file, of type store_chain;store_location;product_name;product_price,
// as input parameters. The function opens the indicated file, parses the fields separated by a semicolon,
// and adds each field to the map data structure. The function returns false if the file can not be read, or
// true if the file is read, the data is stored and the file closed successfully.
bool csv_to_map_tree(const string& input_file,
                     map<string, map<string, map<string, double>>>& tree){

    ifstream file_object(input_file);

    if (!file_object) {
        // False is returned if file cannot be read.
        return false;

    } else {
        string row;
        Splitter splitter_object(row);

        while (getline(file_object, row)) {
            splitter_object.set_string_to_split(row);
            splitter_object.split(';');

            double price = stod (splitter_object.fetch_field(3)); //convert string to double

            add_product(tree,
                        splitter_object.fetch_field(0),
                        splitter_object.fetch_field(1),
                        splitter_object.fetch_field(2),
                        price);

        }
        file_object.close();
        return true;
    }
}
