TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    splitter.cpp \
    csv_parser.cpp \
    query_functions.cpp

DISTFILES += \
    products.txt

HEADERS += \
    splitter.hh \
    csv_parser.hh \
    query_functions.hh
