// Module: splitter / file: splitter.hh
// The header file for the splitter module:
// holds the introductions for the class used to split strings and store
// the parts in the fields of the splitter object.
// Author:
//   Johannes Pirhonen, ID: 224613
//   johannes.pirhonen@student.tut.fi
#ifndef SPLITTER_HH
#define SPLITTER_HH


#include <string>
#include <vector>
#include <stdexcept>

using namespace std;

const unsigned int SPLIT_ERROR = 0;

class Splitter {
  public:
    Splitter(const string& string_to_split = "");

    // In addition to the constructor parameter we can also
    // set the string we want to split with this method.
    // The idea is that one Splitter object can be used
    // for splitting different strings at the different times
    // of the program execution.
    void set_string_to_split(const string& string_to_split);

    // The string will be split into parts/fields on a separator.
    // Return value tells how many fields did the string have.
    // If skip_empty_fields is true, then the empty fields
    // resulting from the split are ignored.  This is a useful
    // feature if we are splitting a line of words separated by spaces
    // and we don't know if there is exactly one space between each word:
    //     splitterobject.split(' ', true);
    unsigned int split(char separator, bool skip_empty_fields = false);

    // Returns the number of fields resulting from the
    // previous split operation.  In principle the return
    // value is the same as what the split method returns.
    // There is one exception: return value is SPLIT_ERROR
    // in case the split method has not been called after
    // the string to be split was set (either in the constructor
    // or in the set_string_to_split method).
    unsigned int number_of_fields() const;

    // After the split has been done (using split method)
    // the different fields resulting from the split can be
    // queried with fetch_field method.  The fields are
    // numbered/indexed from zero up to an integer one less than
    // the number of the fields.  If fetch_field is called
    // before split method or if the index is too large
    // compared to the number of fields in the result this
    // method results out_of_range exception:
    //
    //     #include <stdexcept>
    //     Â·Â·Â·
    //     throw out_of_range("too large index or split not called");
    //
    string fetch_field(unsigned int field_index) const;

  private:
    string original_string_;
    vector<string> split_result_;
};
#endif // SPLITTER_HH
