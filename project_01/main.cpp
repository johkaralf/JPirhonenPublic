//----------------------------------------------------------------------//
//                                                                      //
// TIE-02206 Basic Course on Programming, S2017                         //
//                                                                      //
// Programmer:  Johannes Pirhonen                                       //
// Student ID:  224613                                                  //
// E-mail:      johannes.pirhonen@student.tut.fi                        //
// Work:        project-01                                              //
// Modified:    10.10.2017                                              //
//                                                                      //
// This program is used to test and manipulate dates of the Gregorian   //
// calendar. The user can input dates in day-month-year format and get  //
// information as an output. The program validates the users input and  //
// tells wether the date tested is valid or invalid. In the first case  //
// the program outputs the date, the calculated weekday, and tells the  //
// user the previous and the next date in the Gregorian calendar. The   //
// program is terminated by typing the command "quit".                  //
// The purpose of the excercise is to practice classes and objects in   //
// C++. In particular, manipulating objects and understanging methods   //
// and return values.                                                   //
//                                                                      //
//----------------------------------------------------------------------//

#include <iostream>
#include <string>

using namespace std;

// A constant that is used when parsing the user input to identify if a
// character in the input is valid as a date character or not.
const string VALID_DATE_CHARS = "-0123456789";

// The class that allows us to manipulate dates. The format of the date is so
// called Gregorian date. The class is able to store and manipulate Gregorian
// format dates. The public interface offer methods for:
//      Creating and initializing a date object.
//      Setting the value of a date object so that nonexisting dates will be rejected.
//      A separate method for each to find out the date object's day, month, and year number.
//      Printing the date stored in the object on the screen.
//      Incrementing and decrementing the date by one day.
//      What weekday is the date stored in the object.
// Private fields:
//      Julian Day Number
class Date {
  public:
    Date(int D, int M, int Y);
    bool set_date(string input);
    void increment_by_one_day();
    void decrement_by_one_day();
    void print(string input);

    int fetch_day() const;
    int fetch_month() const;
    int fetch_year() const;
    string fetch_weekday();
    string fetch_date();

  private:
    // Days since the beginning of the Julian Period.
    int julian_day_number_;
};

// The main function implemented to test the class Date.
int main() {
    Date date(0,0,0);
    string user_input = "";
    cout << "--------------------------------" << endl
         << "Welcome to date testing program!" << endl
         << "--------------------------------" << endl
         << endl
         << "Input a date in day-month-year format: ";
    getline(cin, user_input);
    while(user_input != "quit") {
        cout << "--- TEST OUTPUT BEGIN" << endl;
        date.print(user_input);
        cout << "--- TEST OUTPUT END" << endl
             << endl
             << "Input a date in day-month-year format: ";
        getline(cin, user_input);
    }
    cout << endl
         << "Test program ending, goodbye!" << endl;
    return 0;
}

// The constructor for the class. The date object is initialized by
// calculating the days since the beginning of the Julian Period.
// Parameters:
//      D: Day of the month
//      M: Month of the year
//      Y: Year
Date::Date(int D, int M, int Y):
    julian_day_number_((1461 * (Y + 4800 + (M - 14)/12))/4
                       + (367 * (M - 2 - 12 * ((M - 14)/12)))/12
                       - (3 * ((Y + 4900 + (M - 14)/12)/100))/4 + D - 32075) {
}

// A method that is used to change the private field julian_day_number_
// from user input. This method also checks for invalid characters and
// user input errors. When a error is encountered, the method returns false.
// If the input has no wrong characters, the method continues to check if
// the date is valid, calculates the julian_day_number_ from the Gregorian
// style date and finally returns true.
//
// Parameters:
//     User input date string: Gregorian style date (day-month-year)
// Returns:
//     True or false
bool Date::set_date(string input_date) {
    string day;
    string month;
    string year;
    int separator_count = 0;
    string::size_type found = 0;

    // Here the user input characters are parsed to form separate
    // strings for day, month and year.
    for (char& c : input_date) {
        found = VALID_DATE_CHARS.find(c);
        if (found != string::npos) {
            if (c == '-') {
                ++separator_count;
            }
            else if (separator_count == 0) {
                day += c;
            }
            else if (separator_count == 1) {
                month += c;
            }
            else if (separator_count == 2) {
                year += c;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // This if-statement does an initial check for invalid inputs.
    // The set_date-method returns false if the above for-loop failed
    // in parsing the user input to all of the strings day, month and
    // year.
    if (day.size() < 1 || month.size() < 1 || year.size() < 1 || year.size() > 4) {
        return false;
    }

    // The date strings are converted to integers and the validity of them
    // is checked via a switch function for each month.
    int D = stoi(day);
    int M = stoi(month);
    int Y = stoi(year);
    switch (M) {
      case 1:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 2:
        if (D == 29){
            // Calculation for the verification of the leap year.
            if ((((Y % 4) == 0) && ((Y % 100) != 0)) || ((Y % 400) == 0)){
                break;
            } else {
                return false;
            }
        }
        else if (1 <= D && D <= 28){
            break;
        } else {
            return false;
        }
      case 3:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 4:
        if (1 <= D && D <= 30){
            break;
        } else {
            return false;
        }
      case 5:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 6:
        if (1 <= D && D <= 30){
            break;
        } else {
            return false;
        }
      case 7:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 8:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 9:
        if (1 <= D && D <= 30){
            break;
        } else {
            return false;
        }
      case 10:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      case 11:
        if (1 <= D && D <= 30){
            break;
        } else {
            return false;
        }
      case 12:
        if (1 <= D && D <= 31){
            break;
        } else {
            return false;
        }
      default:
        return false;
    }

    // The formula for calculating a julian day number is ripped from:
    // https://en.wikipedia.org/wiki/Julian_day
    julian_day_number_ = (1461 * (Y + 4800 + (M - 14)/12))/4
            + (367 * (M - 2 - 12 * ((M - 14)/12)))/12
            - (3 * ((Y + 4900 + (M - 14)/12)/100))/4 + D - 32075;
    return true;
}

void Date::increment_by_one_day() {
    ++julian_day_number_;
}

void Date::decrement_by_one_day() {
    --julian_day_number_;
}

// This method calculates the Gregorian style day from a given Julian Day Number.
// The algorithm is ripped from:
// https://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
// Returns:
//      day
int Date::fetch_day() const {
    float Q = julian_day_number_ + 0.5;
    int Z = Q;
    int W = (Z - 1867216.25) / 36524.25;
    int X = W / 4;
    int A = Z + 1 + W - X;
    int B = A + 1524;
    int C = (B - 122.1) / 365.25;
    int D = 365.25 * C;
    int E = (B - D) / 30.6001;
    int F = 30.6001 * E;

    int day = B-D-F+(Q-Z);
    return day;

}

// This method calculates the Gregorian style month from a given Julian Day Number.
// The algorithm is ripped from:
// https://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
// Returns:
//      month
int Date::fetch_month() const {
    float Q = julian_day_number_ + 0.5;
    int Z = Q;
    int W = (Z - 1867216.25) / 36524.25;
    int X = W / 4;
    int A = Z + 1 + W - X;
    int B = A + 1524;
    int C = (B - 122.1) / 365.25;
    int D = 365.25 * C;
    int E = (B - D) / 30.6001;
    //int F = 30.6001 * E;

    int month = E - 1;
    if (month > 12) {
        month = E - 13;
    }
    return month;
}

// This method calculates the Gregorian style year from a given Julian Day Number.
// The algorithm is ripped from:
// https://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
// Returns:
//      year
int Date::fetch_year() const {
    float Q = julian_day_number_ + 0.5;
    int Z = Q;
    int W = (Z - 1867216.25) / 36524.25;
    int X = W / 4;
    int A = Z + 1 + W - X;
    int B = A + 1524;
    int C = (B - 122.1) / 365.25;
    int D = 365.25 * C;
    int E = (B - D) / 30.6001;
    //int F = 30.6001 * E;

    int month = E - 1;
    if (month > 12) {
        month = E - 13;
    }
    int year;
    if (month == 1 || month == 2) {
        year = C - 4715;
    } else {
        year = C - 4716;
    }
    return year;
}

// The method for calculating the weekday from the Julian Day Number
// The formula for calculating a julian day number is ripped from:
// https://en.wikipedia.org/wiki/Julian_day
// Returns: Day of the week
string Date::fetch_weekday() {
    int weekday_number = julian_day_number_ % 7;

    switch (weekday_number) {
      case 0:
        return "Monday";
      case 1:
        return "Tuesday";
      case 2:
        return "Wednesday";
      case 3:
        return "Thursday";
      case 4:
        return "Friday";
      case 5:
        return "Saturday";
      case 6:
        return "Sunday";
      default:
        return "???";
    }
}

// The method that concatenates the date information and
// Returns: Gregorian date in "Weekday day.month.year" form.
string Date::fetch_date() {

    string date = fetch_weekday() + " "
            + to_string(fetch_day()) + "."
            + to_string(fetch_month()) + "."
            + to_string(fetch_year());
    return date;
}

// The print-method utilizes the helper methods fetch_day,
// fetch_month and fetch_year to print out the day, month and
// year respectively, from the calculated Julian Day Number.
// Also part of the test outputs are printed here.
void Date::print(string date_input){
    if (set_date(date_input)) {
        cout << "The date is a valid date." << endl;
    } else {
        cout << "Error: this is not a valid date!" << endl;
       return;
    }
    cout << "Day is " << fetch_day() << endl
         << "Month is " << fetch_month() << endl
         << "Year is " << fetch_year() << endl
         << "Weekday is " << fetch_weekday() << endl;
    decrement_by_one_day();
    cout << "The previous date was: " << fetch_date() << endl;
    increment_by_one_day();
    increment_by_one_day();
    cout << "The next date will be: " << fetch_date() << endl;
}
